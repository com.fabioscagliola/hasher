# Hasher 

Hasher is a Windows application that computes the MD5 hash of a string 

![Hasher 1.0 screenshot](https://fabioscagliola.com/images/Hasher.1.0.png) 

As the content of the **String** text area changes, the hash is automatically updated

Hasher requires the Microsoft .NET Framework 4.6 

[Hasher.1.0.zip](https://gitlab.com/com.fabioscagliola/hasher/uploads/41f1c005e1398337ca392a3590cbaace/Hasher.1.0.zip) 

